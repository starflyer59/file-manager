import { FileData } from "./types";

export const fileData: FileData[] = [
  {
    id: "4ca2dc63-9ecd-4cbe-b582-016473665b2c",
    name: "smss.exe",
    device: "Mario",
    path: "\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe",
    status: "scheduled",
  },
  {
    id: "1684aa82-5b08-4500-86ca-54b83082d33e",
    name: "netsh.exe",
    device: "Luigi",
    path: "\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe",
    status: "available",
  },
  {
    id: "688c1e6e-43d2-4a8c-ac41-d373e6fb7761",
    name: "uxtheme.dll",
    device: "Peach",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll",
    status: "available",
  },
  {
    id: "75bcb7bb-20eb-4977-868c-7655c2c45815",
    name: "aries.sys",
    device: "Daisy",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\aries.sys",
    status: "scheduled",
  },
  {
    id: "b30b491d-9d71-4479-88b8-9a2e8823804e",
    name: "cryptbase.dll",
    device: "Yoshi",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll",
    status: "scheduled",
  },
  {
    id: "216e33f7-6615-4011-809b-d5ba4b3c26b8",
    name: "7za.exe",
    device: "Toad",
    path: "\\Device\\HarddiskVolume1\\temp\\7za.exe",
    status: "scheduled",
  },
];
