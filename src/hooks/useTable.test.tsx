import { renderHook, act } from "@testing-library/react";
import "@testing-library/jest-dom";
import useTable from "./useTable";

type TestFileData = {
  id: string;
  status: string;
};

describe("useTable hook", () => {
  const initialData: TestFileData[] = [
    { id: "1", status: "active" },
    { id: "2", status: "inactive" },
    { id: "3", status: "active" },
  ];

  const isSelectable = (item: TestFileData) => item.status === "active";

  test("initially, no items are selected", () => {
    const { result } = renderHook(() => useTable(initialData, isSelectable));
    expect(result.current.selectedIds.size).toBe(0);
    expect(result.current.isNoneSelected).toBe(true);
  });

  test("toggleSelect adds/removes an item from the selection", () => {
    const { result } = renderHook(() => useTable(initialData, isSelectable));

    act(() => {
      result.current.toggleSelect("1");
    });

    expect(result.current.selectedIds.has("1")).toBe(true);

    act(() => {
      result.current.toggleSelect("1");
    });

    expect(result.current.selectedIds.has("1")).toBe(false);
  });

  test("toggleSelectAll selects all selectable items or clears selection", () => {
    const { result } = renderHook(() => useTable(initialData, isSelectable));

    act(() => {
      result.current.toggleSelectAll();
    });

    expect(result.current.selectedIds.size).toBe(2);
    expect(result.current.isAllSelected).toBe(true);

    act(() => {
      result.current.toggleSelectAll();
    });

    expect(result.current.selectedIds.size).toBe(0);
    expect(result.current.isNoneSelected).toBe(true);
  });

  test("selecting a non-selectable item does nothing", () => {
    const { result } = renderHook(() => useTable(initialData, isSelectable));

    act(() => {
      result.current.toggleSelect("2");
    });

    expect(result.current.selectedIds.has("2")).toBe(false);
  });
});
