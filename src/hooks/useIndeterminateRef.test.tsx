import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { useRef } from "react";
import useIndeterminateRef from "./useIndeterminateRef";

const TestableCheckbox = ({ indeterminate }: { indeterminate: boolean }) => {
  const ref = useRef<HTMLInputElement>(null);
  useIndeterminateRef({ ref, indeterminate });

  return <input type="checkbox" ref={ref} />;
};

describe("useIndeterminateRef hook", () => {
  test("sets the indeterminate prop", () => {
    render(<TestableCheckbox indeterminate={true} />);
    const checkbox = screen.getByRole("checkbox");

    expect(checkbox).toHaveProperty("indeterminate", true);
  });

  test("clears the indeterminate prop", () => {
    const { rerender } = render(<TestableCheckbox indeterminate={true} />);
    rerender(<TestableCheckbox indeterminate={false} />);

    const checkbox = screen.getByRole("checkbox");

    expect(checkbox).toHaveProperty("indeterminate", false);
  });
});
