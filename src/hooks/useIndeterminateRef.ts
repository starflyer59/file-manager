import { useEffect, RefObject } from "react";

const useIndeterminateRef = ({
  ref,
  indeterminate,
}: {
  ref: RefObject<HTMLInputElement>;
  indeterminate?: boolean;
}) => {
  useEffect(() => {
    if (ref.current) {
      ref.current.indeterminate = Boolean(indeterminate);
    }
  }, [indeterminate, ref]);
  return ref;
};

export default useIndeterminateRef;
