import { useState } from "react";

function useTable<TItem extends { id: string; status?: string }>(
  data: TItem[],
  isSelectable: (item: TItem) => boolean,
) {
  const [selectedIds, setSelectedIds] = useState<Set<string>>(new Set());

  const toggleSelectAll = () => {
    if (selectedIds.size === data.filter(isSelectable).length) {
      setSelectedIds(new Set());
    } else {
      const newSelected = new Set(
        data.filter(isSelectable).map((item) => item.id),
      );
      setSelectedIds(newSelected);
    }
  };

  const toggleSelect = (id: string) => {
    const targetItem = data.find((item) => item.id === id);
    if (!targetItem || !isSelectable(targetItem)) {
      return; // we'll ignore non-selectable items
    }

    const newSelected = new Set(selectedIds);
    if (newSelected.has(id)) {
      newSelected.delete(id);
    } else {
      newSelected.add(id);
    }
    setSelectedIds(newSelected);
  };

  return {
    selectedIds,
    toggleSelectAll,
    toggleSelect,
    isAllSelected: selectedIds.size === data.filter(isSelectable).length,
    isNoneSelected: selectedIds.size === 0,
  };
}

export default useTable;
