import { InputHTMLAttributes, useRef } from "react";
import useIndeterminateRef from "../../hooks/useIndeterminateRef";

interface CheckboxProps extends InputHTMLAttributes<HTMLInputElement> {
  indeterminate?: boolean;
}

const Checkbox = ({ checked, indeterminate, ...props }: CheckboxProps) => {
  const ref = useRef<HTMLInputElement>(null);
  const inputRef = useIndeterminateRef({
    ref,
    indeterminate: indeterminate,
  });

  return (
    <input
      ref={inputRef}
      type="checkbox"
      checked={checked}
      aria-checked={indeterminate ? "mixed" : checked}
      {...props}
    />
  );
};

export default Checkbox;
