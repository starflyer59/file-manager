import styled from "@emotion/styled";

const SelectedWrapper = styled.div`
  width: 120px;
`;

export default SelectedWrapper;
