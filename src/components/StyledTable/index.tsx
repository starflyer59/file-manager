import styled from "@emotion/styled";
import { HTMLAttributes } from "react";

interface TRProps extends HTMLAttributes<HTMLTableRowElement> {
  selected?: boolean;
}

export const TableComponent = styled.table`
  border-collapse: collapse;
`;

export const TH = styled.th`
  border-bottom: 1px solid #ddd;
  padding: 8px;
  text-align: left;
`;

export const TR = styled.tr<TRProps>`
  background-color: ${(props) => (props.selected ? "#e7e7e7" : undefined)};
  border-bottom: 1px solid #ddd;

  &:hover {
    background-color: #f1f1f1;
    cursor: pointer;
  }

  &:last-child {
    border-bottom: none;
  }
`;

export const TD = styled.td<HTMLAttributes<HTMLTableDataCellElement>>`
  text-align: left;
  padding: 8px;

  &:has(.status) {
    padding: 0;

    & > div {
      display: flex;
      justify-content: flex-end;
      align-items: center;
    }
  }

  & .status {
    border-radius: 8px;
    width: 16px;
    height: 16px;

    &.available {
      background-color: green;
    }
  }
`;
