import styled from "@emotion/styled";

const Container = styled.div`
  border: 1px solid #ddd;
  display: flex;
  flex-direction: column;
`;

export default Container;
