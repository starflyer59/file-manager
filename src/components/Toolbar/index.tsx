import styled from "@emotion/styled";

const Toolbar = styled.div`
  border-bottom: 1px solid #ddd;
  display: flex;
  align-content: flex-start;
  align-items: center;
  gap: 16px;
  padding: 8px;
  text-align: left;
`;

export default Toolbar;
