import { HTMLAttributes, ReactNode } from "react";
import { TableComponent, TH, TR, TD } from "../StyledTable";

interface TableCellProps extends HTMLAttributes<HTMLTableCellElement> {
  children: ReactNode;
}
interface TableRowProps extends HTMLAttributes<HTMLTableRowElement> {
  children: ReactNode;
  selected?: boolean;
}

const Table = ({ children, ...props }: { children: ReactNode }) => (
  <TableComponent {...props}>{children}</TableComponent>
);

Table.Header = ({ children, ...props }: { children: ReactNode }) => (
  <thead {...props}>{children}</thead>
);
Table.Row = ({ children, ...props }: TableRowProps) => (
  <TR {...props}>{children}</TR>
);
Table.HeaderCell = ({ children, ...props }: { children?: ReactNode }) => (
  <TH {...props}>{children}</TH>
);
Table.Cell = ({ children, ...props }: TableCellProps) => (
  <TD {...props}>{children}</TD>
);

export default Table;
