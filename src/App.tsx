import "./App.css";
import { FileData } from "./types";
import { fileData } from "./constants";
import { ReactComponent as DownloadIcon } from "./assets/download.svg";
import useTable from "./hooks/useTable";
import Checkbox from "./components/Checkbox";
import Container from "./components/Container";
import SelectedWrapper from "./components/SelectedWrapper";
import Table from "./components/Table";
import Toolbar from "./components/Toolbar";

function App() {
  const {
    selectedIds,
    toggleSelectAll,
    toggleSelect,
    isAllSelected,
    isNoneSelected,
  } = useTable(fileData, (file: FileData) => file.status === "available");

  const isIndeterminate = !isAllSelected && !isNoneSelected;

  const handleDownload = () => {
    const downloadFiles = fileData.filter((item) => selectedIds.has(item.id));

    const message = `Downloading items 
    ${downloadFiles
      .map((file) => {
        return `\nDevice Name: ${file.device}\nPath: ${file.path}\n`;
      })
      .join("")}`;

    alert(message);
  };

  return (
    <>
      <Container>
        <Toolbar>
          <Checkbox
            data-testid="select-all"
            indeterminate={isIndeterminate}
            checked={isAllSelected}
            onChange={() => toggleSelectAll()}
          />

          <SelectedWrapper data-testid="selected-items">
            {selectedIds.size > 0 ? (
              <span>Selected {selectedIds.size}</span>
            ) : (
              <span>None selected</span>
            )}
          </SelectedWrapper>

          <button
            data-testid="download-selected"
            disabled={isNoneSelected}
            onClick={handleDownload}
          >
            <DownloadIcon /> Download Selected
          </button>
        </Toolbar>

        <Table data-testid="filemanager-table">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Device</Table.HeaderCell>
              <Table.HeaderCell>Path</Table.HeaderCell>
              <Table.HeaderCell></Table.HeaderCell>
              <Table.HeaderCell>Status</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <tbody>
            {fileData.map((file) => {
              const isSelected = selectedIds.has(file.id);

              return (
                <Table.Row
                  key={file.id}
                  selected={isSelected}
                  aria-selected={isSelected}
                  onClick={() => toggleSelect(file.id)}
                  onKeyDown={(e) => {
                    if (e.key === " ") {
                      e.preventDefault();
                      toggleSelect(file.id);
                    }
                  }}
                >
                  <Table.Cell>
                    <Checkbox
                      data-testid={`checkbox-${file.id}`}
                      type="checkbox"
                      checked={isSelected}
                      onChange={() => toggleSelect(file.id)}
                    />
                  </Table.Cell>
                  <Table.Cell tabIndex={0}>{file.name}</Table.Cell>
                  <Table.Cell tabIndex={0}>{file.device}</Table.Cell>
                  <Table.Cell tabIndex={0}>{file.path}</Table.Cell>
                  <Table.Cell tabIndex={-1}>
                    <div>
                      <div className={`status ${file.status}`} />
                    </div>
                  </Table.Cell>
                  <Table.Cell tabIndex={0}>{file.status}</Table.Cell>
                </Table.Row>
              );
            })}
          </tbody>
        </Table>
      </Container>
    </>
  );
}

export default App;
