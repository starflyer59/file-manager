import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import App from "./App";
import { fileData } from "./constants";

describe("Main App component", () => {
  test("renders without crashing", () => {
    render(<App />);

    // verify a few main items are in the app
    expect(screen.getByTestId("select-all")).toBeInTheDocument();
    expect(screen.getByTestId("download-selected")).toBeInTheDocument();
    expect(screen.getByTestId("filemanager-table")).toBeInTheDocument();
  });

  test("The select-all checkbox should be in an unselected state if no items are selected", () => {
    render(<App />);

    const selectAllCheckbox = screen.getByTestId("select-all");
    expect(selectAllCheckbox).not.toBeChecked();
  });

  test("The select-all checkbox should be in a selected state if all items are selected", () => {
    render(<App />);

    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const selectAllCheckbox = screen.getByTestId("select-all");

    // select all selectable items
    selectableFileData.forEach((file) => {
      const checkbox = screen.getByTestId(`checkbox-${file.id}`);
      fireEvent.click(checkbox);
    });

    // assert "select-all" is checked
    expect(selectAllCheckbox).toBeChecked();
  });

  test("The select-all checkbox should be in an indeterminate state if some but not all items are selected", () => {
    render(<App />);

    const selectAllCheckbox = screen.getByTestId("select-all");

    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];
    const firstCheckbox = screen.getByTestId(`checkbox-${firstFile.id}`);

    // select the first file
    fireEvent.click(firstCheckbox);

    // assert the select-all checkbox is indeterminate and has correct aria-checked value
    expect(selectAllCheckbox).toHaveProperty("indeterminate", true);
    expect(selectAllCheckbox).toHaveAttribute("aria-checked", "mixed");
  });

  test("The 'selected-items' text should reflect the count of selected items and display 'None Selected' when there are none selected.", () => {
    render(<App />);

    expect(screen.getByTestId("selected-items")).toHaveTextContent(
      "None selected",
    );

    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];
    const firstCheckbox = screen.getByTestId(`checkbox-${firstFile.id}`);
    fireEvent.click(firstCheckbox);

    // assert 1 selected item
    expect(screen.getByTestId("selected-items")).toHaveTextContent(
      "Selected 1",
    );

    const secondFile = selectableFileData[1];
    const secondCheckbox = screen.getByTestId(`checkbox-${secondFile.id}`);
    fireEvent.click(secondCheckbox);

    // assert 2 selected items
    expect(screen.getByTestId("selected-items")).toHaveTextContent(
      "Selected 2",
    );

    // deselect the first available file
    fireEvent.click(firstCheckbox);

    // assert 1 selected item
    expect(screen.getByTestId("selected-items")).toHaveTextContent(
      "Selected 1",
    );
  });

  test('Clicking the "select-all" checkbox should select all items if none or some are selected', async () => {
    render(<App />);

    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );

    const selectAllCheckbox = screen.getByTestId("select-all");
    expect(selectAllCheckbox).not.toBeChecked();

    fireEvent.click(selectAllCheckbox);

    // assert "select-all" is checked
    expect(selectAllCheckbox).toBeChecked();

    // assert all selectable items are selected
    selectableFileData.forEach((file) => {
      expect(screen.getByTestId(`checkbox-${file.id}`)).toBeChecked();
    });

    // deselect one item
    const firstFile = selectableFileData[0];
    const firstCheckbox = screen.getByTestId(`checkbox-${firstFile.id}`);
    fireEvent.click(firstCheckbox);
    expect(selectAllCheckbox).toHaveProperty("indeterminate", true);
    expect(selectAllCheckbox).toHaveAttribute("aria-checked", "mixed");

    // re-select all items
    fireEvent.click(selectAllCheckbox);

    // assert all selectable items are selected again
    selectableFileData.forEach((file) => {
      expect(screen.getByTestId(`checkbox-${file.id}`)).toBeChecked();
    });
  });

  test('Clicking the "select-all" checkbox should deselect all items if all are currently selected', async () => {
    render(<App />);

    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );

    const selectAllCheckbox = screen.getByTestId("select-all");

    // initial state
    expect(selectAllCheckbox).not.toBeChecked();

    fireEvent.click(selectAllCheckbox);
    // assert "select-all" is checked
    expect(selectAllCheckbox).toBeChecked();

    // assert all selectable items are selected
    selectableFileData.forEach((file) => {
      expect(screen.getByTestId(`checkbox-${file.id}`)).toBeChecked();
    });

    // deselect all items
    fireEvent.click(selectAllCheckbox);

    // assert all selectable items are not selected
    selectableFileData.forEach((file) => {
      expect(screen.getByTestId(`checkbox-${file.id}`)).not.toBeChecked();
    });
  });

  test('Clicking "Download Selected" when some or all items are displayed should generate an alert box with the path and device of all selected files', () => {
    render(<App />);

    // global alert mock
    global.alert = jest.fn();

    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];
    const firstCheckbox = screen.getByTestId(`checkbox-${firstFile.id}`);
    fireEvent.click(firstCheckbox);

    // click download
    const downloadButton = screen.getByTestId("download-selected");
    fireEvent.click(downloadButton);

    // asset alert has correct message items
    expect(global.alert).toHaveBeenCalledWith(
      expect.stringContaining(firstFile.device),
    );
    expect(global.alert).toHaveBeenCalledWith(
      expect.stringContaining(firstFile.path),
    );
  });
});
