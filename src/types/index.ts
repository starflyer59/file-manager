export interface FileData {
  id: string;
  name: string;
  device: string;
  path: string;
  status: "available" | "scheduled";
}
