import { fileData } from "../../../src/constants";

describe("App Tests", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it('The "select-all" checkbox should be in an unselected state if no items are selected', () => {
    cy.get('[data-testid="select-all"]').should("not.be.checked");
  });

  it('The "select-all" checkbox should be in a selected state if all items are selected', () => {
    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );

    selectableFileData.forEach((file) => {
      cy.get(`[data-testid="checkbox-${file.id}"]`).click();
    });

    cy.get('[data-testid="select-all"]').should("be.checked");
  });

  it("The select-all checkbox should be in an indeterminate state if some but not all items are selected", () => {
    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];

    cy.get(`[data-testid="checkbox-${firstFile.id}"]`).click();

    cy.get('[data-testid="select-all"]').should(
      "have.attr",
      "aria-checked",
      "mixed",
    );
  });

  it("The 'selected-items' text should reflect the count of selected items and display 'None Selected' when there are none selected.", () => {
    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];
    const secondFile = selectableFileData[1];

    // none selected
    cy.get('[data-testid="selected-items"]').should(
      "have.text",
      "None selected",
    );

    // one selected
    cy.get(`[data-testid="checkbox-${firstFile.id}"]`).click();
    cy.get('[data-testid="selected-items"]').should("have.text", "Selected 1");

    // two selected
    cy.get(`[data-testid="checkbox-${secondFile.id}"]`).click();
    cy.get('[data-testid="selected-items"]').should("have.text", "Selected 2");

    // one deselected
    cy.get(`[data-testid="checkbox-${firstFile.id}"]`).click();
    cy.get('[data-testid="selected-items"]').should("have.text", "Selected 1");
  });

  it('Clicking the "select-all" checkbox should select all items if none or some are selected', () => {
    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];

    // select all
    cy.get('[data-testid="select-all"]').click();

    // assert "select-all" is checked
    cy.get('[data-testid="select-all"]').should("be.checked");

    // assert all selectable items are selected
    selectableFileData.forEach((file) => {
      cy.get(`[data-testid="checkbox-${file.id}"]`).should("be.checked");
    });

    // deselect one item
    cy.get(`[data-testid="checkbox-${firstFile.id}"]`).click();
    cy.get(`[data-testid="checkbox-${firstFile.id}"]`).should("not.be.checked");
    cy.get('[data-testid="select-all"]').should(
      "have.attr",
      "aria-checked",
      "mixed",
    );

    // re-select all items
    cy.get('[data-testid="select-all"]').click();

    // assert all selectable items are selected again
    selectableFileData.forEach((file) => {
      cy.get(`[data-testid="checkbox-${file.id}"]`).should("be.checked");
    });
  });

  it('Clicking the "select-all" checkbox should deselect all items if all are currently selected', () => {
    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );

    // initial state
    cy.get('[data-testid="select-all"]').should("not.be.checked");

    // assert "select-all" is checked
    cy.get('[data-testid="select-all"]').click();
    cy.get('[data-testid="select-all"]').should("be.checked");

    // assert all selectable items are selected
    selectableFileData.forEach((file) => {
      cy.get(`[data-testid="checkbox-${file.id}"]`).should("be.checked");
    });

    // deselect all items
    cy.get('[data-testid="select-all"]').click();

    // assert all selectable items are not selected
    selectableFileData.forEach((file) => {
      cy.get(`[data-testid="checkbox-${file.id}"]`).should("not.be.checked");
    });
  });

  it('Clicking "Download Selected" when some or all items are displayed should generate an alert box with the path and device of all selected files', () => {
    const selectableFileData = fileData.filter(
      (file) => file.status === "available",
    );
    const firstFile = selectableFileData[0];

    // window.alert stud
    cy.window().then((win) => {
      cy.stub(win, "alert").as("windowAlert");
    });

    cy.get(`[data-testid="checkbox-${firstFile.id}"]`).click();
    cy.get('[data-testid="download-selected"]').click();

    // check that alert was fired
    cy.get("@windowAlert").should("have.been.calledOnce");

    // check the content of the alert
    cy.get("@windowAlert").then((stub) => {
      const alertMessage = stub.args[0][0];
      expect(alertMessage).to.include(firstFile.device);
      expect(alertMessage).to.include(firstFile.path);
    });
  });
});
