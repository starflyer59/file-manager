# file-manager exercise

Building a simple file manager application using React. 
* Using a table hook to manage the state of selected files and folders.
* Using [@emotion/styled](https://emotion.sh/docs/styled) for styling and component encapsulation.
* Tests are implement in Jest and Cypress.

## Getting Started
* `npm i`
* `npm start`

## Tests
* `npm test`

## e2e Tests
* `npm run start`
* `npm run cy:run` or `npm run cy:open`
